import os
import random
from dice import Dice

print ("\n\n\n********* PIRATE GAME *********\n")


def getName():
    name = raw_input("Quel est votre nom ? : ").strip()
    os.system('cls')
    print "Salut, %s." % name
    return name


def lancerDice(min, max):
    os.system('cls')
    return random.randint(min, max)

getName()
tours = int(input("\nCombien de tours voulez-vous ? "))
count = 0
points = 0
repeat = True
os.system('cls')

while repeat:
    reponse = raw_input("Voulez-vous Jouer ou Passer (j / p) ? : ").strip().lower()
    os.system('cls')
    if count != tours:
        if reponse == "j":
            print("Jouons !")
            lancer = lancerDice(1, 6)
            print("Resultat du de : "+ str(lancer))
            if lancer == 1 :
                count = -1
                points = 0
            elif lancer > 1 :
                points = points + lancer
        elif reponse == "p":
            print("Passons !")
        elif reponse == "exit":
            break
        else :
            print("J ou P... pas autre chose !")
    else:
        break
    count = count + 1
    os.system('cls')
    print ("Tour : " + str(count))
    print("Nombre de points actuels : "+str(points))

