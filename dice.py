#coding : utf-8

from random import randint

class Dice(object):
    """ Classe Dice """

    def __init__(self):
        """ Initialisation du dé """
        self.face_number = 6
        self.current_value = 0
        self.roll_count = 0

    def roll(face_number):
        self.current_value = random.randint(1, face_number)

    def reset_roll_count(self):
        print "Reset roll"
        _set_roll(0)


    """GETTERS AND SETTERS""" 

    """ ****** FACE NUMBER ****** """
    @face_number.setter 
    def set_face_number(self, number):
        self.face_number = number

    @property
    def get_face_number(self):
        """ Get face_number """
        return self.face_number

    """ ****** ROLL COUNT ****** """
    @property
    def _get_roll(self):
        """Get roll count"""
        return self.roll_count

    @roll_count.setter
    def _set_roll(self, count):
        """Set roll count"""
        self.roll_count = count

    """ ****** CURRENT VALUE ****** """
       @property
    def _get_current_value(self):
        """Get current value"""
        return self.current_value

    @current_value.setter
    def _set_roll(self, count):
        """Set roll count"""
        self.roll_count = count

    roll_count = property(_get_roll, _set_roll)